const requestJson = require('request-json');
const io = require('../io');
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcsb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountsByIdV2(req, res) {
  console.log("GET /apitechu/v2/accounts/:id");

  var id = req.params.id;
  var query = 'q={"userId":'+ id +'}';
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado.");

   httpClient.get(
     "account?" + query + "&" + mLabAPIKey,
     function(err,resMLab,body) {
       if(err) {
         var response = {"msg": "Error obteniendo cuentas"};
         res.status(500);
       }
       res.send(body);
     }
   )
}

module.exports.getAccountsByIdV2 = getAccountsByIdV2;
