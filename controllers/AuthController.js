const requestJson = require('request-json');
const io = require('../io');
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcsb/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var index = users.findIndex(function(element) {
     return (element.email == req.body.email);
   }
  )
  var result = { };
  if (index == -1) {
    result.msg = "Login incorrecto.";
  } else {
    console.log("Usuario encontrado.");
    var user = users[index];
    console.log(user);
    if(users[index].password == req.body.password) {
      console.log("Password correcto.");
      result.msg = "Login correcto.";
      result.idUsuario = users[index].id;
      user.logged = true;

      users[index] = user;
      console.log(users[index]);
      io.writeUserDataToFile(users);
    } else {
      console.log("Password incorrecto.");
      result.msg = "Login incorrecto.";
    }
  }

  console.log("Proceso login terminado.")

  res.send(result);
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout/:id");
  console.log("Logout:" + req.params.id);

  var users = require('../usuarios.json');
  var index = users.findIndex(function(element) {
     return (element.id == req.params.id);
   }
  )
  var result = {
    "msg":""
  };
  if (index == -1) {
    result.msg = "Logout incorrecto.";
  } else {
    console.log("Usuario encontrado.");
    var user = users[index];
    console.log(user);
    if(user.logged) {
      delete user.logged;
      users[index] = user;
      console.log(users[index]);
      io.writeUserDataToFile(users);
      result.msg = "Logout correcto.";
      result.idUsuario = users[index].id;
    } else {
      result.msg = "Logout incorrecto.";
    }


  }

  console.log("Proceso logout terminado.")

  res.send(result);
}


function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var query = 'q={"email":"'+ req.body.email +'"}';
  console.log("La consulta es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body) {
      var response = {};
      console.log(body);
       if(body.length > 0) {
         var usuario = body[0];
         if(crypt.checkPassword(req.body.password, usuario.password)) {
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err,resMLab,body) {
             console.log(body);
             response.msg = "Login correcto";
             response.id = usuario.id;
             res.send(response);
           })
         } else {
           response.msg = "Login incorrecto";
           res.status(401);
           res.send(response);
         }
       } else {
         response.msg = "Login incorrecto";
         res.status(401);
         res.send(response);
       }

    }
  )
}


function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");
  console.log("Logout:" + req.params.id);

  var query = 'q={"id":'+ req.params.id +'}';
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body) {
      var response = {};
      if(body.length > 0) {
        if(body[0].logged) {
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(err,resMLab,body) {
            console.log(body);
            response.msg = "Logout correcto";
            res.send(response);
          })
        } else {
          response.msg = "Logout incorrecto";
          res.send(response);
        }

      } else {
        response.msg = "Logout incorrecto";
        res.send(response);
      }
    })
}



module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
