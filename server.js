console.log("Hola Mundo");

require('dotenv').config();
//Indicamos que necesitamos la dependencia express
const express = require('express');
//Iniciamos express
const app = express();
//Indicamos que por defecto tome las peticiones como json
app.use(express.json());

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//Definimos puerto (variable del sistema, si no existe 3000)
const port = process.env.PORT || 3000;

//Habilita controllers
var enableCORS = function(req,res,next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS);

//Inicia el servidor en el puerto indicado
app.listen(port);
console.log("Api escuchando en el puerto " + port);


app.get("/apitechu/v1/hello",
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechu"});
  }
)

app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);
app.delete("/apitechu/v2/users/:id", userController.deleteUserV2);
app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v1/logout/:id", authController.logoutV1);
app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);
app.get("/apitechu/v2/accounts/:id", accountController.getAccountsByIdV2);



app.post("/apitechu/v1/monstruo/:param1/:param2",
  function(req, res) {
    console.log("POST /apitechu/v1/monstruo/:param1/:param2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)
