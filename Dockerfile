# Dockerfile

# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de local a Imagen
ADD . /apitechu

# Instalacion de las dependencias
RUN npm install --only=prod

# Puerto que vamos a usar
EXPOSE 3000

# Comando de inicializacion
CMD ["node", "server.js"]
